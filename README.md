# list-view

[![Build Status](https://travis-ci.com/pinyin/list-view.svg?branch=master)](https://travis-ci.com/pinyin/list-view)

A list view with animation &amp; virtualization support.

## Install 

`npm install @pinyin/list-view --save`

[Demo](http://pinyin.github.io/list-view/)

## Usage

``` typescript jsx
import {ListView} from '@pinyin/list-view'

<ListView range={{from: xxx, to: xxx}}>
// Elements
</ListView>
```

The `<ListView>` component roughly accepts the same props as a `div`, with a few differences.

For more information, please see the [source code of the demo](https://github.com/pinyin/list-view/blob/master/demo/Demo.tsx).

## Limits

Some passed styles will be ignored.

## Demo

[Infinite scrolling component implemented with this component](http://pinyin.github.io/list-view/)

Or clone this repo and run `npm run demo` at its root directory to open a local demo.

## License

MIT
