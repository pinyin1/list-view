import {endPhase} from '@pinyin/frame'
import {ItemID, ItemSpec, ListLayout, ListLayoutImpl} from '@pinyin/list-layout'
import {assume, ensure, existing} from '@pinyin/maybe'
import {Measure} from '@pinyin/measure'
import {PropsOf} from '@pinyin/react'
import {ComponentWithRedux} from '@pinyin/react-with-redux'
import {Action, NamedReducers} from '@pinyin/redux'
import {getTweenState, tweenExit, tweenHere, TweenState} from '@pinyin/tween-here'
import {EventHandler, MatchTag, ObjExclude, PayloadOf, px} from '@pinyin/types';
import * as React from 'react'
import {CSSProperties} from 'react'
import {applyMiddleware} from 'redux'
import {composeWithDevTools} from 'redux-devtools-extension'
import {createRxMiddleware} from 'redux-middleware-rxjs'
import {from} from 'rxjs';
import {bufferWhen, filter, map} from 'rxjs/operators'
import {uuidv4} from './uuidv4'
import equal = require('fast-deep-equal')

class ListViewComponent extends ComponentWithRedux<Props, State, Actions, Snapshot> {
    state = {
        layout: ListLayoutImpl.create(
            getIDsFromProps(this.props),
            assume(this.props.initialHeight, it => it > 0 ? it : 200) || 200,
            this.props.predefinedHeights
        )
    }

    shouldComponentUpdate(nextProps: Props, nextState: State): boolean {
        if (nextProps !== this.props) {
            return true
        }
        return nextState.layout !== this.state.layout
    }

    getSnapshotBeforeUpdate(nextProps: Props, nextState: State): Snapshot {
        const {items} = this.elements
        const itemSnapshots = new Map()

        items.forEach((item, id) => {
            const state = getTweenState(item)
            itemSnapshots.set(id, state)
            tweenExit(item, curr => ({...curr, opacity: 0}), 300)
        })

        return {itemSnapshots}
    }

    private styles = {
        root: (): CSSProperties => {
            const {state, props} = this
            const {layout} = state
            return {
                width: `auto`,
                willChange: `height`,
                ...props.style,
                height: `${layout.height}px`,
                position: 'relative'
            }
        },
        item: (spec: ItemSpec): CSSProperties => {
            return {
                position: `absolute`,
                top: `${spec.offset}px`,
                width: `auto`,
                zIndex: spec.index,
                willChange: `top, transform`
            }
        }
    }
    private elements = {
        items: new Map<ItemID, HTMLDivElement>()
    }

    render() {
        const {layout} = this.state
        const {
            children,
            range, innerRef,
            onListLayoutUpdate, initialHeight, predefinedHeights, // prevent these from reaching <div/>
            ...rootProps
        } = this.props
        const childrenArr = React.Children.toArray(children)
        const {handle, elements, styles} = this

        const segments = layout.getByOffsetRange(range.from, range.to)

        return <div {...rootProps} style={styles.root()} ref={innerRef}>
            {segments.map(segment =>
                <Measure key={segment.id} onResize={handle.onResize}>
                    <div style={styles.item(segment)} data-id={segment.id}
                         ref={ref => existing(ref) ?
                             elements.items.set(segment.id, ref) :
                             elements.items.delete(segment.id)}>
                        {childrenArr[segment.index]}
                    </div>
                </Measure>
            )}
        </div>
    }

    componentDidUpdate(prevProps: Props, prevState: State, snapshot: Snapshot) {
        const {props, state, dispatch, elements} = this
        const {itemSnapshots} = snapshot

        if (prevProps !== props && !equal(getIDsFromProps(prevProps), getIDsFromProps(props))) {
            dispatch[ActionTypes.ItemsUpdated](getIDsFromProps(props))
        }

        elements.items.forEach((element, id) =>
            tweenHere(
                element,
                curr => (itemSnapshots.get(id) || {...curr, opacity: 0}),
                200
            )
        )

        if (existing(props.onListLayoutUpdate)) {
            props.onListLayoutUpdate(state.layout)
        }
    }

    private handle = {
        onResize: (resize: Array<ResizeObserverEntry>) => {
            if (resize.length < 1) {
                return
            }
            const record = resize[resize.length - 1]
            const {target, contentRect} = record

            const id = (target as HTMLElement).dataset.id as string
            const height = contentRect.height

            this.dispatch[ActionTypes.ItemResized]({id, height})
        }
    }

    protected readonly reducers: NamedReducers<State, Actions> = {
        [ActionTypes.ItemResized]: state => state,
        [ActionTypes.LayoutUpdated]: (state, payload) => {
            const updates: Map<ItemID, px> =
                new Map(payload.map(({id, height}) => [id, height] as [ItemID, px]))
            const layout = state.layout.updateWith(updates)

            return {layout}
        },
        [ActionTypes.ItemsUpdated]: (state, payload) => {
            const layout = state.layout.deriveFrom(payload)

            return {layout}
        }
    }

    enhancer = composeWithDevTools({
        name: `${ListView.displayName} ${ensure(this.props.id, `${uuidv4()}`)}`
    })(applyMiddleware(createRxMiddleware<ListAction>(listAction => {
            const resizeAction = listAction.pipe(
                filter(action => action.type === ActionTypes.ItemResized),
                map(action => action.payload as PayloadOf<ListAction<ActionTypes.ItemResized>>)
            )

            return resizeAction.pipe(
                bufferWhen(() => from(endPhase())),
                filter(buffer => buffer.length > 0),
                map(buffer => ({
                    type: ActionTypes.LayoutUpdated,
                    payload: buffer
                } as ListAction<ActionTypes.LayoutUpdated>))
            )
        }
    )))
}

export const ListView = React.forwardRef<HTMLDivElement, ObjExclude<Props, InnerRef>>((props, ref) =>
    <ListViewComponent {...props} innerRef={ref}/>
)
ListView.displayName = `ListView`

export type Props =
    PropsOf<'div'> &
    InnerRef &
    {
        range: RenderRange, children: Array<Item> | Item,
        initialHeight?: px, predefinedHeights?: ReadonlyMap<ItemID, px>
    } &
    { onListLayoutUpdate?: EventHandler<ListLayout> }
export type InnerRef = { innerRef?: React.Ref<HTMLDivElement> }

export type Item = React.ReactElement<{ key: string }>

export type State = Readonly<{
    layout: ListLayout
}>

export type Snapshot = {
    itemSnapshots: Map<ItemID, TweenState>
}

type ListAction<T extends ActionTypes = ActionTypes> = MatchTag<Action<Actions>, T>

enum ActionTypes {
    ItemResized = 'ItemResized',
    LayoutUpdated = 'LayoutUpdated',
    ItemsUpdated = 'ItemsUpdated'
}

type Actions = {
    [ActionTypes.ItemResized]: ResizeEvent
    [ActionTypes.LayoutUpdated]: Array<ResizeEvent>
    [ActionTypes.ItemsUpdated]: Array<ItemID>
}

type ResizeEvent = { id: ItemID, height: px }

export type RenderRange = { from: px, to: px }

function getIDsFromProps(props: Props): Array<ItemID> {
    return React.Children.map(props.children, item => (item as Item).key as string)
}

